package main

import (
	"bitbucket.org/ditoraafiu/application_project/cmd"
)

func main() {
	cmd.Execute()
}
