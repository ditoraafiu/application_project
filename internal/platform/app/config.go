package app

// Config structure of service
type Config struct {
	HTTPServer struct {
		Port         int `mapstructure:"port"`
		ReadTimeout  int `mapstructure:"read_timeout"`
		WriteTimeout int `mapstructure:"write_timeout"`
	} `mapstructure:"http_server"`
	ServiceName string `mapstructure:"service_name"`
	Log         struct {
		Level string `mapstructure:"level"`
	} `mapstructure:"log"`
	SQL struct {
		MaxLifetimeConnection int    `mapstructure:"max_lifetime_connection"`
		Host                  string `mapstructure:"host"`
		Username              string `mapstructure:"username"`
		DBName                string `mapstructure:"db_name"`
		Charset               string `mapstructure:"charset"`
		MaxIdleConnection     int    `mapstructure:"max_idle_connection"`
		Port                  int    `mapstructure:"port"`
		Password              string `mapstructure:"password"`
		MaxOpenConnection     int    `mapstructure:"max_open_connection"`
	} `mapstructure:"sql"`
}
