package app

import (
	"errors"

	"bitbucket.org/ditoraafiu/application_project/internal/platform/sqlpersistence"
	"bitbucket.org/ditoraafiu/application_project/internal/repository"
	"bitbucket.org/ditoraafiu/application_project/internal/service"
	"github.com/spf13/viper"
)

// Context define structure Application Context
type Context struct {
	Config         *Config
	services       map[string]service.Services
	repositories   map[string]repository.Repositories
	SQLPersistence sqlpersistence.Persistence
}

// New service context
func New() *Context {
	ctx := new(Context)
	ctx.services = make(map[string]service.Services)
	ctx.repositories = make(map[string]repository.Repositories)

	return ctx
}

// LoadConfig file to configuration structure
func (c *Context) LoadConfig() error {
	var err error

	viper.SetConfigName("App")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("./configurations")

	err = viper.ReadInConfig()
	if err != nil {
		panic(err)
	}

	return viper.Unmarshal(&c.Config)
}

// SetService to set application service
func (c *Context) SetService(key string, value service.Services) {
	c.services[key] = value
}

// GetService to get application service
func (c *Context) GetService(key string) (service.Services, error) {
	service, ok := c.services[key]
	if !ok {
		return nil, errors.New("undefined key " + key)
	}
	return service, nil
}

// SetRepository to set application repository
func (c *Context) SetRepository(key string, value repository.Repositories) {
	c.repositories[key] = value
}

// GetRepository to get application repository
func (c *Context) GetRepository(key string) (repository.Repositories, error) {
	repository, ok := c.repositories[key]
	if !ok {
		return nil, errors.New("undefined key " + key)
	}
	return repository, nil
}
