package responses

import "net/http"

// APIErrorValidation constant error response
var APIErrorValidation = API{
	HTTPStatus: http.StatusBadRequest,
	Code:       4000,
	Message:    "missing required parameter(s)",
}

// APIErrorInternal constant error response
var APIErrorInternal = API{
	HTTPStatus: http.StatusInternalServerError,
	Code:       5000,
	Message:    "internal server error",
}

// APIErrorUnauthenticate constant error response
var APIErrorUnauthenticate = API{
	HTTPStatus: http.StatusUnauthorized,
	Code:       4100,
	Message:    "unauthorized request",
}
