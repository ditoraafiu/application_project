package responses

import "net/http"

// API response structure
type API struct {
	HTTPStatus int         `json:"-"`
	Code       int         `json:"code"`
	Message    string      `json:"message,omitempty"`
	Data       interface{} `json:"data,omitempty"`
}

// SetData API response
func (a API) SetData(data interface{}) API {
	a.Message = ""
	a.Data = data

	return a
}

// GeneralSuccess constant response
var GeneralSuccess = API{
	HTTPStatus: http.StatusOK,
	Code:       1000,
	Message:    "Ok",
	Data:       nil,
}
