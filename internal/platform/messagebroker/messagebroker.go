package messagebroker

import "context"

// Config structure
type Config struct {
	Hosts        []string
	Username     string
	Password     string
	DeleteUnused bool
	Exclusive    bool
}

// ConsumerHandlerFunc interface
type ConsumerHandlerFunc func(context.Context, []byte) error

// CleanupActionFunc define cleanup action function
type CleanupActionFunc func(context.Context)

// Consumer broker interface
type Consumer interface {
	SetHandlerFunc(handlerFunc ConsumerHandlerFunc)
	SetCleanupAction(cleanupAction CleanupActionFunc)
	Subscribe(ctx context.Context, groupID string, topics []string) error
}

// Producer define producer message interface
type Producer interface {
	Publish(ctx context.Context, topic string, message []byte) error
}
