package gosql

import (
	"context"
	"database/sql"
	"strconv"
	"time"

	// inject go-sql-drive
	_ "github.com/go-sql-driver/mysql"

	"bitbucket.org/ditoraafiu/application_project/internal/platform/sqlpersistence"
)

// Connection structure for go-sql
type Connection struct {
	config *sqlpersistence.Config
	conn   *sql.DB
}

// NewConnection go-sql
func NewConnection(config *sqlpersistence.Config) (*Connection, error) {
	source := config.Username + ":" + config.Password + "@tcp(" + config.Host + ":" + strconv.Itoa(config.Port) + ")/" + config.DBName + "?charset=" + config.Charset + "&parseTime=true&loc=Local"

	conn, err := sql.Open("mysql", source)
	if err != nil {
		return nil, err
	}

	maxLifetime := time.Duration(config.MaxLifetimeConnection) * time.Second
	conn.SetConnMaxLifetime(maxLifetime)
	conn.SetMaxOpenConns(config.MaxOpenConnection)
	conn.SetMaxIdleConns(config.MaxIdleConnection)

	err = conn.Ping()
	if err != nil {
		return nil, err
	}

	return &Connection{
		config: config,
		conn:   conn,
	}, nil
}

// Query data on database
func (c *Connection) Query(ctx context.Context, query string, args ...interface{}) (*sql.Rows, error) {
	return c.conn.QueryContext(ctx, query, args...)
}

// QueryRow data on database
func (c *Connection) QueryRow(ctx context.Context, query string, args ...interface{}) (*sql.Row, error) {
	return c.conn.QueryRowContext(ctx, query, args...), nil
}

// Exec query to database
func (c *Connection) Exec(ctx context.Context, query string, args ...interface{}) (sql.Result, error) {
	return c.conn.ExecContext(ctx, query, args...)
}

// BeginTx query database
func (c *Connection) BeginTx(ctx context.Context, opts *sql.TxOptions) (sqlpersistence.Tx, error) {
	tx, err := c.conn.BeginTx(ctx, opts)
	if err != nil {
		return nil, err
	}

	return &Tx{tx: tx}, nil
}
