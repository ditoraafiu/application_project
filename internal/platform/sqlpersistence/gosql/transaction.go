package gosql

import (
	"context"
	"database/sql"
)

// Tx gosql structure
type Tx struct {
	tx *sql.Tx
}

// Query transactional data on database
func (t *Tx) Query(ctx context.Context, query string, args ...interface{}) (*sql.Rows, error) {
	return t.tx.QueryContext(ctx, query, args...)
}

// QueryRow transactional data on database
func (t *Tx) QueryRow(ctx context.Context, query string, args ...interface{}) (*sql.Row, error) {
	return t.tx.QueryRowContext(ctx, query, args...), nil
}

// Exec transactional query to database
func (t *Tx) Exec(ctx context.Context, query string, args ...interface{}) (sql.Result, error) {
	return t.tx.ExecContext(ctx, query, args...)
}

// Commit transactional queries
func (t *Tx) Commit() error {
	return t.tx.Commit()
}

// Rollback transactional queries
func (t *Tx) Rollback() error {
	return t.tx.Rollback()
}
