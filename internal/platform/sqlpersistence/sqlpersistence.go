package sqlpersistence

import (
	"context"
	"database/sql"
)

// Config structure
type Config struct {
	Host                  string
	Port                  int
	Username              string
	Password              string
	Charset               string
	DBName                string
	MaxOpenConnection     int
	MaxIdleConnection     int
	MaxLifetimeConnection int
}

type query interface {
	Query(ctx context.Context, query string, args ...interface{}) (*sql.Rows, error)
	QueryRow(ctx context.Context, query string, args ...interface{}) (*sql.Row, error)
	Exec(ctx context.Context, query string, args ...interface{}) (sql.Result, error)
}

// Persistence sql interface
type Persistence interface {
	query
	BeginTx(ctx context.Context, opts *sql.TxOptions) (Tx, error)
}

// Tx sql interface
type Tx interface {
	query
	Commit() error
	Rollback() error
}
