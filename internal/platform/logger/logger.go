package logger

import (
	"fmt"
	"runtime"
)

// Config define logger configuration strucutre
type Config struct {
	Level string
}

// Field define logger field type
type Field map[string]interface{}

// Logger define logger interface
type Logger interface {
	Debug(msg string, fields ...Field)
	Info(msg string, fields ...Field)
	Warn(msg string, fields ...Field)
	Error(msg string, fields ...Field)
	Fatal(msg string, fields ...Field)
	Panic(msg string, fields ...Field)
}

var log Logger

// SetEngine to set logger engine
func SetEngine(logEngine Logger) {
	log = logEngine
}

func getCallerInfo(fields ...Field) []Field {
	var logField []Field
	_, file, line, _ := runtime.Caller(2)
	if len(fields) > 0 {
		logField = fields
	} else {
		logField = append(logField, make(Field))
	}

	logField[0]["source_code"] = fmt.Sprintf("%s:%d", file, line)
	return logField
}

// Debug to print log with debug level
func Debug(msg string, fields ...Field) {
	if log != nil {
		fields = getCallerInfo(fields...)
		log.Debug(msg, fields...)
	}
}

// Info to print log with info level
func Info(msg string, fields ...Field) {
	if log != nil {
		fields = getCallerInfo(fields...)
		log.Info(msg, fields...)
	}
}

// Warn to print log with warn level
func Warn(msg string, fields ...Field) {
	if log != nil {
		fields = getCallerInfo(fields...)
		log.Warn(msg, fields...)
	}
}

// Error to print log with error level
func Error(msg string, fields ...Field) {
	if log != nil {
		fields = getCallerInfo(fields...)
		log.Error(msg, fields...)
	}
}

// Fatal to print log with fatal level
func Fatal(msg string, fields ...Field) {
	if log != nil {
		fields = getCallerInfo(fields...)
		log.Fatal(msg, fields...)
	}
}

// Panic to print log with panic level
func Panic(msg string, fields ...Field) {
	if log != nil {
		fields = getCallerInfo(fields...)
		log.Panic(msg, fields...)
	}
}
