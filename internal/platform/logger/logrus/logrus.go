package logrus

import (
	"github.com/sirupsen/logrus"

	"bitbucket.org/ditoraafiu/application_project/internal/platform/logger"
)

// Logger structure of logrus
type Logger struct {
	logger *logrus.Logger
}

// New logrus logger
func New(config *logger.Config) (*Logger, error) {
	level, err := logrus.ParseLevel(config.Level)
	if err != nil {
		return nil, err
	}

	log := logrus.New()
	log.SetFormatter(&logrus.JSONFormatter{})
	log.SetLevel(level)

	logger := new(Logger)
	logger.logger = log

	return logger, nil
}

// Debug to print log with debug level
func (l *Logger) Debug(msg string, fields ...logger.Field) {
	entry := logrus.NewEntry(l.logger)
	for _, field := range fields {
		for key, value := range field {
			entry = entry.WithField(key, value)
		}
	}

	entry.Debug(msg)
}

// Info to print log with info level
func (l *Logger) Info(msg string, fields ...logger.Field) {
	entry := logrus.NewEntry(l.logger)
	for _, field := range fields {
		for key, value := range field {
			entry = entry.WithField(key, value)
		}
	}

	entry.Info(msg)
}

// Warn to print log with warn level
func (l *Logger) Warn(msg string, fields ...logger.Field) {
	entry := logrus.NewEntry(l.logger)
	for _, field := range fields {
		for key, value := range field {
			entry = entry.WithField(key, value)
		}
	}

	entry.Warn(msg)
}

// Error to print log with error level
func (l *Logger) Error(msg string, fields ...logger.Field) {
	entry := logrus.NewEntry(l.logger)
	for _, field := range fields {
		for key, value := range field {
			entry = entry.WithField(key, value)
		}
	}

	entry.Error(msg)
}

// Fatal to print log with fatal level
func (l *Logger) Fatal(msg string, fields ...logger.Field) {
	entry := logrus.NewEntry(l.logger)
	for _, field := range fields {
		for key, value := range field {
			entry = entry.WithField(key, value)
		}
	}

	entry.Fatal(msg)
}

// Panic to print log with panic level
func (l *Logger) Panic(msg string, fields ...logger.Field) {
	entry := logrus.NewEntry(l.logger)
	for _, field := range fields {
		for key, value := range field {
			entry = entry.WithField(key, value)
		}
	}

	entry.Panic(msg)
}
