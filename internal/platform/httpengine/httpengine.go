package httpengine

import "time"

// Config http structure
type Config struct {
	Port         int
	ReadTimeout  time.Duration
	WriteTimeout time.Duration
}

// Engine http interface
type Engine interface {
	SetHandler(interface{}) error
	ListenAndServe() error
}

// Route http structure
type Route struct {
	Path        string
	Method      string
	Middlewares []interface{}
	Handler     interface{}
}
