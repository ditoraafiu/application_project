package httpecho

import (
	"context"
	"errors"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"time"

	"github.com/labstack/echo"
	"github.com/labstack/gommon/log"

	"bitbucket.org/ditoraafiu/application_project/internal/platform/httpengine"
)

// Server define http echo server structure
type Server struct {
	config *httpengine.Config
	server *echo.Echo
}

// NewServer initialize http echo server
func NewServer(config httpengine.Config) *Server {
	server := new(Server)

	server.config = &config
	server.server = echo.New()
	server.server.HideBanner = true
	server.server.Logger.SetLevel(log.WARN)

	return server
}

// SetHandler http echo server handler
func (s *Server) SetHandler(handler interface{}) error {
	r, ok := handler.(httpengine.Route)
	if !ok {
		return errors.New("unsupported route format")
	}

	h, ok := r.Handler.(func(echo.Context) error)
	if !ok {
		return errors.New("unsupported handler format")
	}

	var midds []echo.MiddlewareFunc
	for _, midd := range r.Middlewares {
		m, ok := midd.(func(echo.HandlerFunc) echo.HandlerFunc)
		if !ok {
			return errors.New("unsupported middleware handler format")
		}

		midds = append(midds, m)
	}

	switch r.Method {
	case http.MethodGet:
		s.server.GET(r.Path, h, midds...)
	case http.MethodPost:
		s.server.POST(r.Path, h, midds...)
	case http.MethodPut:
		s.server.PUT(r.Path, h, midds...)
	case http.MethodDelete:
		s.server.DELETE(r.Path, h, midds...)
	}

	return nil
}

// ListenAndServe http server
func (s *Server) ListenAndServe() error {
	var err error

	go func() {
		err = s.server.Start(":" + strconv.Itoa(s.config.Port))
		if err != nil {
			s.server.Logger.Panic(err)
		}
	}()

	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt, os.Kill)
	select {
	case <-quit:
		s.server.Logger.Warn("terminating, via signal.")
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()
		if err := s.server.Shutdown(ctx); err != nil {
			s.server.Logger.Fatal(err)
		}
	}

	s.server.Logger.Warn("server terminated.")

	return nil
}
