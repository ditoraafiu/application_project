package health

import (
	"context"
	"errors"
	"fmt"

	"bitbucket.org/ditoraafiu/application_project/internal/platform/app"
	"bitbucket.org/ditoraafiu/application_project/internal/platform/constants"
)

// Check structure
type Check struct{}

// NewHealthcheck service
func NewHealthcheck(appCtx *app.Context) error {
	var service = new(Check)

	appCtx.SetService(constants.HealthcheckService, service)
	return nil
}

// CheckParameter structure
type CheckParameter struct{}

// Serve healthsvc request
func (h *Check) Serve(ctx context.Context, params ...interface{}) interface{} {
	if len(params) != 1 {
		return errors.New("missing required parameter")
	}

	param, ok := params[0].(*CheckParameter)
	if !ok {
		return errors.New("invalid parameter type")
	}

	fmt.Println(param)

	return nil
}
