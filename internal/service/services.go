package service

import "context"

// Services interface
type Services interface {
	Serve(ctx context.Context, param ...interface{}) interface{}
}
