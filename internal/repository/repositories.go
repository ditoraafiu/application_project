package repository

import "context"

// Repositories interface
type Repositories interface {
	Query(ctx context.Context, param ...interface{}) error
}
