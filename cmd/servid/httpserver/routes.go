package httpserver

import (
	"net/http"

	"bitbucket.org/ditoraafiu/application_project/cmd/servid/httpserver/handler"
	"bitbucket.org/ditoraafiu/application_project/internal/platform/app"
	"bitbucket.org/ditoraafiu/application_project/internal/platform/httpengine"
)

func initRoutes(appCtx *app.Context) ([]httpengine.Route, error) {
	healthHandler, err := handler.NewHealth(appCtx)
	if err != nil {
		return nil, err
	}

	var routes = []httpengine.Route{
		{
			Path:    "/v1",
			Method:  http.MethodGet,
			Handler: healthHandler.Check,
		},
	}

	return routes, nil
}
