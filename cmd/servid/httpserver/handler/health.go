package handler

import (
	"context"
	"net/http"

	"github.com/labstack/echo"

	"bitbucket.org/ditoraafiu/application_project/internal/platform/app"
	"bitbucket.org/ditoraafiu/application_project/internal/platform/constants"
	"bitbucket.org/ditoraafiu/application_project/internal/service"
)

// Health handler structure
type Health struct {
	healthService service.Services
}

// NewHealth handler
func NewHealth(appCtx *app.Context) (*Health, error) {
	healthService, err := appCtx.GetService(constants.HealthcheckService)
	if err != nil {
		return nil, err
	}

	return &Health{
		healthService: healthService,
	}, nil
}

// Check will handle health check request
func (h *Health) Check(c echo.Context) error {
	err := h.healthService.Serve(context.Background(), nil)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, "I'm not fine")
	}

	return c.JSON(http.StatusOK, "I'm fine!!")
}
