package httpserver

import (
	"bitbucket.org/ditoraafiu/application_project/internal/platform/app"
	"bitbucket.org/ditoraafiu/application_project/internal/platform/httpengine"
	"bitbucket.org/ditoraafiu/application_project/internal/platform/httpengine/httpecho"
	"bitbucket.org/ditoraafiu/application_project/internal/platform/logger"
	"bitbucket.org/ditoraafiu/application_project/internal/platform/logger/logrus"
	"bitbucket.org/ditoraafiu/application_project/internal/platform/sqlpersistence"
	"bitbucket.org/ditoraafiu/application_project/internal/platform/sqlpersistence/gosql"
	"bitbucket.org/ditoraafiu/application_project/internal/service/health"
)

// HTTP define structure http server
type HTTP struct {
	appCtx *app.Context
	server httpengine.Engine
}

// NewHTTPServer to initialize http server
func NewHTTPServer() (*HTTP, error) {
	var (
		err    error
		appCtx = app.New()
	)

	h := new(HTTP)
	h.appCtx = appCtx
	err = h.appCtx.LoadConfig()
	if err != nil {
		return nil, err
	}

	err = initLogEngine(appCtx)
	if err != nil {
		return nil, err
	}

	_, err = initGoSQL(appCtx)
	if err != nil {
		return nil, err
	}

	err = h.prepareDependecy()
	if err != nil {
		return nil, err
	}

	err = h.initServer()
	if err != nil {
		return nil, err
	}

	return h, nil
}

func (h *HTTP) prepareDependecy() error {
	// Prepare service and repository dependency

	err := health.NewHealthcheck(h.appCtx)
	if err != nil {
		return err
	}

	return nil
}

func (h *HTTP) initServer() error {
	var httpconfig httpengine.Config
	httpconfig.Port = h.appCtx.Config.HTTPServer.Port

	h.server = httpecho.NewServer(httpconfig)

	routes, err := initRoutes(h.appCtx)
	if err != nil {
		return err
	}

	for _, r := range routes {
		h.server.SetHandler(r)
	}

	return nil
}

// Run to run http server
func (h *HTTP) Run() {
	var err error

	err = h.server.ListenAndServe()
	if err != nil {
		panic(err)
	}
}

func initLogEngine(appCtx *app.Context) error {
	logConfig := logger.Config{
		Level: appCtx.Config.Log.Level,
	}

	engine, err := logrus.New(&logConfig)
	if err != nil {
		return err
	}
	logger.SetEngine(engine)

	return nil
}

func initGoSQL(appCtx *app.Context) (sqlpersistence.Persistence, error) {
	sqlConfig := sqlpersistence.Config{
		Host:                  appCtx.Config.SQL.Host,
		Port:                  appCtx.Config.SQL.Port,
		Username:              appCtx.Config.SQL.Username,
		Password:              appCtx.Config.SQL.Password,
		DBName:                appCtx.Config.SQL.DBName,
		Charset:               appCtx.Config.SQL.Charset,
		MaxOpenConnection:     appCtx.Config.SQL.MaxOpenConnection,
		MaxIdleConnection:     appCtx.Config.SQL.MaxIdleConnection,
		MaxLifetimeConnection: appCtx.Config.SQL.MaxLifetimeConnection,
	}

	conn, err := gosql.NewConnection(&sqlConfig)
	if err != nil {
		return nil, err
	}

	return conn, nil
}
