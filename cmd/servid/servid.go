package servid

import (
	"github.com/spf13/cobra"

	"bitbucket.org/ditoraafiu/application_project/cmd/servid/httpserver"
)

var commands = []*cobra.Command{

	{
		Use:   "httpserver",
		Short: "HTTP Server",
		Long:  "Running HTTP Server service",
		Run: func(cmd *cobra.Command, args []string) {

			server, err := httpserver.NewHTTPServer()
			if err != nil {
				panic(err)
			}

			server.Run()
		},
	},
}

// SetCommands function to set grpc commands
func SetCommands(rootCmd *cobra.Command) {
	for _, command := range commands {
		rootCmd.AddCommand(command)
	}
}
