package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"

	"bitbucket.org/ditoraafiu/application_project/cmd/servid"
)

// Execute service command
func Execute() {
	var rootCmd = &cobra.Command{
		Use:   "./application_project",
		Short: "application_project",
		Long:  "application_project service",
	}

	servid.SetCommands(rootCmd)

	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
